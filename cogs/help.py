#!/usr/bin/env python3

import discord
from discord.ext import commands

class help(commands.Cog):

    def __init__(self,client):
        self.client = client

        @client.group(invoke_without_command=True)
        async def help(ctx):
            await ctx.message.delete()
            help = discord.Embed(title = "Help", description = "Use .help <command> for extended information on a command.", color=ctx.author.color)

            help.add_field(name = "Moderation :lock:", value="Set of commands for moderation",inline=False)
            help.add_field(name="Info :mag:", value="Set of commands for information gathering",inline=False)
            help.add_field(name = "Fun :partying_face:", value="Comming Soon",inline=False)

            await ctx.send(embed = help)

        @help.command()
        async def moderation(ctx):
            moderation = discord.Embed(title = "Moderation Commands", color=ctx.author.color,description="Set of commands to moderate an discord server")
            moderation.add_field(name="Kick", value="`kick <mention> [reason]", inline=False)
            moderation.add_field(name="Ban", value=".ban <mention> [reason]", inline=False)
            moderation.add_field(name="Clear", value=".clear <num_of_messages>`", inline=False)
            moderation.add_field(name="Load", value=".load <command>", inline=False)
            moderation.add_field(name="Unload", value=".unload <command>",inline=False)
            await ctx.send(embed=moderation)


        @help.command()
        async def info(ctx):
            info = discord.Embed(title ="Informating Gathering Commands", color=ctx.author.color, description="Set of tools to ping and lookup IP addresses or hostnames")
            info.add_field(name="Pinger", value=".pinger <IP>",inline=False)
            info.add_field(name="IP", value=".ip <IP>",inline=False)

            await ctx.send(embed = info)

        @help.command()
        async def fun(ctx):
            fun = discord.Embed(title = "Fun Commands", color=ctx.author.color, description="Genral Fun Commands!")
            fun.add_field(name="ASCII", value=".ascii <some text here>", inline=False)
            await ctx.send(embed = fun)

def setup(client):
    client.add_cog(help(client))
