#!/usr/bin/env python3

import discord
from discord.ext import commands


class clear(commands.Cog):

    def __init__(self,client):
        self.client = client

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clear(self,ctx,amount: int):
        await ctx.channel.purge(limit=amount)


    @clear.error
    async def clear_error(self,ctx,error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.message.delete()
            await ctx.send(f"{ctx.message.author.mention} You Do Not Have The Required Permissions To Use This Command!")
        elif isinstance(error,commands.MissingRequiredArgument):
            await ctx.message.delete()
            await ct.sendn(f'{ctx.message.author.mention} Please Specify An Amount Of Messages to Delete!')


def setup(client):
    client.add_cog(clear(client))
