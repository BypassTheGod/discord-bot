import discord
from discord.ext import commands
from datetime import datetime
class kick(commands.Cog):

    def __init__(self,client):
        self.client = client

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self,ctx,member : discord.Member,*, reason=None):
        await ctx.message.delete()
        await member.kick(reason=reason)
        kick = discord.Embed(title=f"Kicked {member} :wave:", color=ctx.author.color)
        kick.add_field(name="Kicked User", value = member, inline=False)
        kick.add_field(name="Reason", value = reason, inline=False)
        kick.set_author(name=ctx.message.author, icon_url=ctx.message.author.avatar_url)
        kick.timestamp = datetime.utcnow()
        await ctx.send(embed = kick)


    @kick.error
    async def kick_error(self,ctx,error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.message.delete()
            await ctx.send(f" {ctx.message.author.mention} You Don't Have Permission To Kick Members")


def setup(client):
    client.add_cog(kick(client))
