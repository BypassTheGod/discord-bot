#!/usr/bin/env python3

import discord
from discord.ext import commands
from datetime import datetime



class ban(commands.Cog):

    def __init__(self,client):
        self.client = client


    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self,ctx,member : discord.Member,*,reason=None):
        await ctx.message.delete()
        await member.ban(reason=reason)
        ban = discord.Embed(title=f'Banned {member} :hammer:', color=ctx.author.color)
        ban.add_field(name="Banned User", value= member, inline=False)
        ban.add_field(name="Reason", value = reason, inlune=False)
        ban.set_author(name=ctx.message.author, icon_url=ctx.message.author.avatar_url)
        ban.timestamp = datetime.utcnow()

        await ctx.send(embed=ban)

    @ban.error
    async def ban_error(self,ctx,error):
        if isinstance(error,commands.MissingPermissions):
            await ctx.message.delete()
            await ctx.send(f"{ctx.message.author.mention} You Don't Have Permission To Ban Members.")

def setup(client):
    client.add_cog(ban(client))
