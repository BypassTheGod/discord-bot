#!/usr/bin/env python3
import discord,requests,json
from discord.ext import commands
from datetime import datetime

class ip(commands.Cog):

    def __init__(self,client):
        self.client = client

    @commands.command()
    @commands.cooldown(1,15, commands.BucketType.user)
    async def ip(self,ctx,*,args):
        await ctx.message.delete()
        ip = requests.get(f"https://ipinfo.io/{args}/json")
        ip_json = ip.json()
        embed = discord.Embed(title=f"IP Lookup - {args}", color=0x329da8)
        embed.add_field(name="IP", value=ip_json["ip"], inline=False)
        embed.add_field(name="ISP", value=ip_json["org"], inline=False)
        embed.add_field(name="Region", value=ip_json["region"],inline=False)
        embed.add_field(name="City", value=ip_json["city"],inline=False)
        embed.add_field(name="Country", value=ip_json["country"],inline=False)
        embed.timestamp = datetime.utcnow()
        await ctx.send(embed = embed, delete_after=15)


    @ip.error
    async def ip_error(self,ctx,error):
        if isinstance(error,commands.CommandOnCooldown):
            await ctx.message.delete()
            em = discord.Embed(title=f"Slow it down!", description=f"Try again in {error.retry_after:.2f}s {ctx.author.mention}", color=ctx.author.color)
            await ctx.send(embed=em)
        elif isinstance(error,commands.MissingRequiredArgument):
            await ctx.message.delete()
            await ctx.send(f"{ctx.message.author.mention} You Did Not Pass Any Arguments")


def setup(client):
    client.add_cog(ip(client))
