#!/usr/bin/env python3



import discord, requests, json, subprocess
from discord.ext import commands



class pinger(commands.Cog):

    def __init__(self,client):
        self.client = client


    @commands.command()
    @commands.cooldown(1,120, commands.BucketType.user)
    async def pinger(self,ctx,*,args):
        async with ctx.typing():
            await ctx.message.delete()
            pinger_ip = subprocess.run(f"ping -c 4 {args}", shell=True,stdout=subprocess.PIPE)
            ping_results = pinger_ip.stdout.decode()
            await ctx.send(f"```{ping_results}```",delete_after=10)


    @pinger.error
    async def pinger_error(self,ctx,error):
        if isinstance(error,commands.CommandOnCooldown):
            await ctx.message.delete()
            em = discord.Embed(title=f"Slow it down!", description=f"Try again in {error.retry_after:.2f}s {ctx.author.mention}", color=ctx.author.color)
            await ctx.send(embed=em)
        elif isinstance(error,commands.MissingRequiredArgument):
            await ctx.message.delete()
            await ctx.send(f"{ctx.message.author.mention} You Did Not Pass Any Arguments")

def setup(client):
    client.add_cog(pinger(client))
