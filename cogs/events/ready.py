#!/usr/bin/env python3

import discord
from discord.ext import commands


class on_ready(commands.Cog):

    def __init__(self,client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("-" * 40)
        print("logged in as", self.client.user)
        print("-" * 40)


def setup(client):
    client.add_cog(on_ready(client))
