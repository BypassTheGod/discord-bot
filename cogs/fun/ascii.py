#!/usr/bin/env python3

import discord
from discord.ext import commands
import pyfiglet

class ascii(commands.Cog):

    def __init__(self,client):
        self.client = client


    @commands.command()
    @commands.cooldown(1,5, commands.BucketType.user)
    async def ascii(self,ctx,*,args):
        async with ctx.typing():
            await ctx.message.delete()
            text = pyfiglet.figlet_format(args)
            await ctx.send(f"```{text}```")


    @ascii.error
    async def ascii_error(self,ctx,error):
        if isinstance(error,commands.CommandOnCooldown):
            await ctx.message.delete()
            em = discord.Embed(title=f"Slow it down!", description=f"Try again in {error.retry_after:.2f}s {ctx.author.mention}", color=ctx.author.color)
            await ctx.send(embed=em)
        elif isinstance(error,commands.MissingRequiredArgument):
            await ctx.message.delete()
            await ctx.send(f"{ctx.message.author.mention} You Did Not Pass Any Arguments")

def setup(client):
    client.add_cog(ascii(client))
