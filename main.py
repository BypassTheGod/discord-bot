import discord
from discord.ext import commands
import os

intents = discord.Intents().default()
intents.members = True

client = commands.Bot(command_prefix = ".", intents=intents)

client.remove_command("help")

#load cogs

#unload cogs

#loads folders for commands and events

for filename in os.listdir("./cogs"):
    if filename.endswith(".py"):
        client.load_extension(f'cogs.{filename[:-3]}')

for cogs in os.listdir("./cogs/mod/"):
    if cogs.endswith(".py"):
        client.load_extension(f"cogs.mod.{cogs[:-3]}")

for info in os.listdir("./cogs/info/"):
    if info.endswith(".py"):
        client.load_extension(f"cogs.info.{info[:-3]}")

for events in os.listdir("./cogs/events/"):
    if events.endswith(".py"):
        client.load_extension(f"cogs.events.{events[:-3]}")

for fun in os.listdir("./cogs/fun/"):
    if fun.endswith(".py"):
        client.load_extension(f"cogs.fun.{fun[:-3]}")

@client.command()
async def load(ctx,extension):
    await ctx.send("Comming Soon (WIP)")

@client.command()
async def unload(ctx,extension):
    await ctx.send("Comming Soon (WIP)")

client.run("TOKEN-HERE")
